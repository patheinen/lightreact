import Fonts from './Fonts'
import Metrics from './Metrics'
import Colors from './Colors'
import {Image} from 'react-native'
import {Dimensions} from 'react-native';

let CARD_PREVIEW_WIDTH = Metrics.marginEverywhere
let CARD_MARGIN = 0.25 * Metrics.marginEverywhere;
let CARD_WIDTH = Dimensions.get('window').width - (CARD_MARGIN + CARD_PREVIEW_WIDTH) * 2;

// This file is for a reusable grouping of Theme items.
// Similar to an XML fragment layout in Android

const ApplicationStyles = {

  screen: {
    mainContainer: {
      flex: 1,
      marginTop: 0, //Metrics.navBarHeight,
      backgroundColor: Colors.transparent
    },
    backgroundImage: {
      position: 'absolute',
      top: 0,
      left: 0,
      bottom: 0,
      right: 0,
      resizeMode: Image.resizeMode.stretch,
      width: null, //Metrics.screenWidth
      height: null, //Metrics.screenHeight
    },
    container: {
      flex: 1,
      marginTop: Metrics.navBarHeight,
      paddingTop: 0.5 * Metrics.marginEverywhere,
      paddingBottom: Metrics.marginEverywhere
    },
    section: {
      margin: Metrics.section,
      padding: Metrics.baseMargin,
      borderTopColor: Colors.frost,
      borderTopWidth: 0.5,
      borderBottomColor: Colors.transparent,
      borderBottomWidth: 1
    },
    sectionText: {
      color: Colors.snow,
      marginVertical: Metrics.smallMargin,
      textAlign: 'center',
      flex: 1,
      justifyContent: 'center',
      fontWeight: 'bold'
    },
    errorContainer: {
      backgroundColor: Colors.tangerine,
      flex: 1,
      position: 'absolute',
      justifyContent: 'center',
      alignItems: 'center',
      width: Metrics.screenWidth,
      bottom: Metrics.screenHeight * 0,
      minHeight: Metrics.screenHeight * 0.1
    },
    errorText: {
      ...Fonts.style.error,
      color: Colors.snow,
      marginVertical: Metrics.smallMargin,
      textAlign: 'center',
      flex: 1,
      justifyContent: 'center'
    },
    subtitle: {
      ...Fonts.style.light,
      color: Colors.snow,
      padding: Metrics.smallMargin,
      marginBottom: Metrics.smallMargin,
      marginHorizontal: Metrics.smallMargin,
      textAlign: 'center'
    },
    renderHeadingText: {
      ...Fonts.style.centeredSubHeading,
      color: Colors.snow,
      margin: Metrics.marginEverywhere
    },
    mainTextContent: {
      ...Fonts.style.mainTextContent,
      color: Colors.snow,
      flex: 1,
      marginLeft: Metrics.marginEverywhere,
      marginRight: Metrics.marginEverywhere,
      marginTop: Metrics.marginEverywhere,
      marginBottom: Metrics.marginEverywhere
    },
    mainTextContentBold: {
      ...Fonts.style.mainTextContentBold,
      color: Colors.snow,
      flex: 1,
      marginLeft: Metrics.marginEverywhere,
      marginRight: Metrics.marginEverywhere,
      marginTop: Metrics.marginEverywhere,
      marginBottom: Metrics.marginEverywhere
    },
    arrowImage: {

      resizeMode: Image.resizeMode.contain,
      width: 10, //Metrics.screenWidth
      height: 10, //Metrics.screenHeight
    },
    circleView: {
      margin: 5,
      flexDirection: 'column'
    },

    circleTextInner: {
      ...Fonts.style.sleepInfoTagDash,
      color: Colors.snow,
      textAlign: 'center'
    },
    circleTextUnder: {
      ...Fonts.style.mainTextContentDash,
      color: Colors.snow,
      textAlign: 'center',
      paddingTop: 10
    },
    overviewInner: {
      paddingBottom: Metrics.marginEverywhere,
      backgroundColor: 'rgba(0,0,0,0.15)',
      borderRadius: 7,
      width: CARD_WIDTH,
      marginLeft: CARD_MARGIN,
      marginRight: CARD_MARGIN,
      alignItems: 'center',
      justifyContent: 'center'
    },
    insightsInner: {
      paddingBottom: Metrics.marginEverywhere,
      backgroundColor: 'rgba(0,0,0,0.15)',
      borderRadius: 7,
      width: CARD_WIDTH,
      marginLeft: CARD_MARGIN,
      marginRight: CARD_MARGIN,
      alignItems: 'flex-start',
      justifyContent: 'center'
    },
    insightsText: {
      ...Fonts.style.centeredSubHeading,
      color: Colors.snow,
      marginLeft: Metrics.marginEverywhere,
      marginRight: Metrics.marginEverywhere,
      marginTop: 10,
      marginBottom: 10
    },
    insightsTextRight: {
      ...Fonts.style.rightSubHeading,
      color: Colors.snow,
      marginLeft: Metrics.marginEverywhere,
      marginRight: Metrics.marginEverywhere,
      marginTop: -2.9 * Metrics.marginEverywhere,
      marginBottom: 1 * Metrics.marginEverywhere
    }
  },
  parts: {},
  darkLabelContainer: {
    backgroundColor: Colors.cloud,
    padding: Metrics.smallMargin
  },
  darkLabel: {
    fontFamily: Fonts.bold,
    color: Colors.snow
  },
  groupContainer: {
    margin: Metrics.smallMargin,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center'
  },
  sectionTitle: {
    ...Fonts.style.h4,
    color: Colors.coal,
    backgroundColor: Colors.ricePaper,
    padding: Metrics.smallMargin,
    marginTop: Metrics.smallMargin,
    marginHorizontal: Metrics.baseMargin,
    borderWidth: 1,
    borderColor: Colors.transparent,
    alignItems: 'center',
    textAlign: 'center'
  },

  goalTextSummary: {
    ...Fonts.style.goalTextSummary,
    color: Colors.snow,
    flex: 1,
    marginLeft: Metrics.marginEverywhere,
    marginRight: Metrics.marginEverywhere,
    marginTop: 22,
    marginBottom: 16
  }
}

export default ApplicationStyles
