const type = {
  base: 'Sansation-Regular',
  bold: 'Sansation-Bold',
  light: 'Sansation-Light',
  emphasis: 'Sansation-Italic'
}

const size = {
  h1: 38,
  h2: 34,
  h3: 30,
  h4: 26,
  h5: 20,
  h6: 16,
  h7: 14,
  input: 18,
  regular: 17,
  medium: 14,
  small: 12,
  tiny: 8.5,
  tab: 16
}

const style = {
  title: {
    fontFamily: type.bold,
    fontSize: 17,
    paddingTop: 2
  },
  titleRight: {
    fontFamily: type.base,
    fontSize: 15,
    paddingTop: 2
  },
  h1: {
    fontFamily: type.base,
    fontSize: size.h1
  },
  h2: {
    fontWeight: 'bold',
    fontSize: size.h2
  },
  h3: {
    fontFamily: type.emphasis,
    fontSize: size.h3
  },
  h4: {
    fontFamily: type.base,
    fontSize: size.h4
  },
  h5: {
    fontFamily: type.base,
    fontSize: size.h5
  },
  h6: {
    fontFamily: type.emphasis,
    fontSize: size.h6
  },
  normal: {
    fontFamily: type.base,
    fontSize: size.regular
  },
  description: {
    fontFamily: type.base,
    fontSize: size.medium
  },
  light: {
    fontFamily: type.light,
    fontSize: size.small
  },
  sleepInfoTag: {
    fontFamily: type.light,
    fontSize: size.medium
  },
  sleepInfoTagDash: {
    fontFamily: type.light,
    fontSize: size.small
  },
  error: {
    fontFamily: type.base,
    fontSize: size.regular
  },
  drawer: {
    fontFamily: type.base,
    fontSize: size.h7
  },
  profileTitle: {
    fontFamily: type.bold,
    fontWeight: 'bold',
    fontSize: size.h4
  },
  centeredSubHeading: {
    fontFamily: type.bold,
    fontWeight: 'bold',
    fontSize: 17,
    textAlign: 'center'
  },
  rightSubHeading: {
    fontFamily: type.base,
    fontSize: 17,
    textAlign: 'right'
  },
  centeredSubHeadingSmall: {
    fontFamily: type.bold,
    fontWeight: 'bold',
    fontSize: size.h6,
    textAlign: 'center'
  },
  insights: {
    fontFamily: type.bold,
    fontWeight: 'bold',
    fontSize: size.h6
  },
  insightsHeading: {
    fontFamily: type.base,
    fontSize: size.h6
  },
  sleepTab: {
    fontFamily: type.base,
    fontSize: size.tab
  },
  sleepTabSelected: {
    fontFamily: type.bold,
    fontWeight: 'bold',
    fontSize: size.tab
  },
  graphXtext: {
    fontFamily: type.base,
    fontSize: size.regular
  },
  graphYtext: {
    fontFamily: type.base,
    fontSize: size.small
  },
  explained: {
    fontFamily: type.base,
    fontSize: size.medium
  },

  mainTextContent: {
    fontFamily: type.base,
    fontSize: 12
  },

  mainTextContentBold: {
    fontFamily: type.bold,
    fontSize: 12
  },
  mainTextContentDash: {
    fontFamily: type.base,
    fontSize: 12
  },
  insightsTextTitle: {
    fontFamily: type.bold,
    fontSize: 14,
    textAlign: 'left'
  },
  goalTextSummary: {
    fontFamily: type.base,
    fontSize: 14,
    textAlign: 'left'
  },
  dashNameText: {
    fontFamily: type.bold,
    fontSize: size.h6
  }
}

export default {
  type,
  size,
  style
}
