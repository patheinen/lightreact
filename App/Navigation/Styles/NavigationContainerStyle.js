import {Colors, Fonts} from '../../Themes/'

export default {
  container : {
    flex: 1
  },
  navBar : {
    backgroundColor: Colors.bloodOrange,
    borderBottomColor: Colors.tangerine,
  },
  navBarHidden : {
    backgroundColor: Colors.transparent,
    height: 0
  },
  title : {
    ...Fonts.style.title,
    color: Colors.snow
  },
  leftButton : {
    tintColor: Colors.snow
  },
  rightButton : {
    ...Fonts.style.titleRight,
    color: Colors.snow
  }
}
