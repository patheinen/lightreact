import React, {Component} from 'react'
import { Router, Scene, Actions } from 'react-native-router-flux';

import PageOne from '../Containers/PageOne';
import PageTwo from '../Containers/PageTwo';
import PageMap from '../Containers/PageMap';
import styles from './Styles/NavigationContainerStyle'

const scenes = Actions.create(
  <Scene key="root" navigationBarStyle={styles.navBar} titleStyle={styles.title}>
    <Scene key="pageOne" component={PageOne} title="PageOne" initial={true} />
    <Scene key="pageTwo" component={PageTwo} title="PageTwoTest" />
  </Scene>
  );

class NavigationRouter extends Component {

  render() {
    return (
          <Router scenes={scenes}/>
    )
  }
}

export default NavigationRouter
