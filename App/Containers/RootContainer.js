import React, {Component} from 'react'
import {View, StatusBar, Image, Text} from 'react-native'
//import {connect} from 'react-redux'
import {Images} from '../Themes'
import NavigationRouter from '../Navigation/NavigationRouter'
import Tabs from 'react-native-tabs';
import {Actions as NavigationActions} from 'react-native-router-flux';

import styles from '../Themes/ApplicationStyles'

class RootContainer extends Component {

    constructor(props) {
        super(props);
        this.state = {
            page: 'first'
        };
    }

    componentDidMount() {
        //this.props.startup()
    }

    clickedTab(el) {
        console.log("Inside clickedTab function")
        this.setState({page: el.props.name})
        console.log(el.props.name);
        //  el.props.selectedAction
        if (el.props.name === "third") {
            console.log("three tried");
        } else if(el.props.name === "second") {
          //NavigationActions.pageList({type: "reset"})
        } else {
          NavigationActions.pageOne({type: "reset"})
        }

    }

    render() {
        console.log("State page is " + this.state.page)
        //const goToTest = () => NavigationActions.pageTwo({text: 'Hello World!'});
        return (
            <View style={styles.screen.mainContainer}>
                {/*<Image source={Images.background} style={styles.backgroundImage} resizeMode='stretch' />*/}
                <StatusBar barStyle='light-content'/>
                <NavigationRouter/>
                <Tabs style={{
                    backgroundColor: 'white'
                }} selectedStyle={{
                    color: 'red'
                }} onSelect={el => this.clickedTab(el)} selected={this.state.page} selectedIconStyle={{
                    borderTopWidth: 2,
                    borderTopColor: 'red'
                }}>
                    <Text name="first">First</Text>
                    <Text name="second">Second</Text>
                    <Text name="third">Third</Text>
                    <Text name="fourth">Fourth</Text>
                    <Text name="fifth">Fifth</Text>
                </Tabs>
            </View>
        )
    }
}

export default RootContainer
