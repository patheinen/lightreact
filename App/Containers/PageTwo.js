import React, { Component } from 'react';
import { View, Text } from 'react-native';
import {connect} from 'react-redux';
import * as actions from '../Actions'

class PageTwo extends Component {

  clickedText() {
    console.log("We have clicked text")
    var  userId = 'paolorovella'

    //dispatch({type: 'USER_FETCH_REQUESTED', payload: {userId}})
    this.props.fetchUser(userId)
  }

  showCorrectButton () {
    console.log("Running this")
    let {userData} = this.props
    if(userData.user && !userData.isFetching) {
      return <Text>Welcome {userData.user.name}</Text>
    } else if(userData.isFetching) {
      return <Text>Loading...</Text>
    } else {
      return <Text onPress={this.clickedText.bind(this)}>Download user</Text>
    }
  }

  render() {
  const {userData} = this.props
  return (
    <View style={{margin: 128}}>
      {this.showCorrectButton()}
      <Text>{this.props.text}</Text>
    </View>
  )
}
}

PageTwo.defaultProps = {
 userData: {},
};

const mapStateToProps = (state) => ({
 userData: state.userState,
});

const mapDispatchToProps = (dispatch) => ({
 fetchUser: (user) => {
   dispatch(actions.fetchUser(user));
 },
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(PageTwo);
